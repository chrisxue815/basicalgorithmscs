﻿using BasicAlgorithmsCs.Matrix;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BasicAlgorithmsCsTest
{
    [TestClass]
    public class TestMatrixUtils
    {
        [TestMethod]
        public void Test()
        {
            const bool T = true;
            const bool F = false;

            var mtx = new[,]
            {
                {T, T, T, T, T, F, F, F},
                {T, F, F, F, T, F, F, F},
                {T, F, F, F, T, F, F, F},
                {T, F, F, F, T, T, T, T},
                {T, T, F, T, T, T, T, T},
                {T, T, F, T, T, F, F, F},
                {T, T, F, F, F, F, F, F},
            };

            var expected = new[,]
            {
                {T, T, T, T, T, F, F, F},
                {T, T, T, T, T, F, F, F},
                {T, T, T, T, T, F, F, F},
                {T, T, T, T, T, T, T, T},
                {T, T, T, T, T, T, T, T},
                {T, T, T, T, T, T, T, T},
                {T, T, T, T, T, T, T, T},
            };

            MatrixUtils.Fill(mtx, 2, 3);

            for (var i = 0; i < mtx.GetLength(0); i++)
            {
                for (var j = 0; j < mtx.GetLength(1); j++)
                {
                    if (mtx[i, j] != expected[i, j]) Assert.Fail();
                }
            }
        }
    }
}
