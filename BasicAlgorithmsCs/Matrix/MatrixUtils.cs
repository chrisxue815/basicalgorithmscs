﻿namespace BasicAlgorithmsCs.Matrix
{
    public class MatrixUtils
    {
        public static void Fill(bool[,] mtx, int i, int j)
        {
            if (mtx[i, j]) return;

            mtx[i, j] = true;

            Advance(mtx, i, j, 1, 0);
            Advance(mtx, i, j, -1, 0);
            Advance(mtx, i, j, 0, 1);
            Advance(mtx, i, j, 0, -1);
        }

        private static void Advance(bool[,] mtx, int i, int j, int iOffset, int jOffset)
        {
            i += iOffset;
            j += jOffset;

            if (i < 0 || i >= mtx.GetLength(0)
                || j < 0 || j >= mtx.GetLength(1)
                || mtx[i, j])
            {
                return;
            }

            mtx[i, j] = true;

            Advance(mtx, i, j, iOffset, jOffset);

            if (iOffset == 0)
            {
                Advance(mtx, i, j, 1, 0);
                Advance(mtx, i, j, -1, 0);
            }
            else
            {
                Advance(mtx, i, j, 0, 1);
                Advance(mtx, i, j, 0, -1);
            }
        }
    }
}
