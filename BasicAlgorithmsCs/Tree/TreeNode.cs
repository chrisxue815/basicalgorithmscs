﻿namespace BasicAlgorithmsCs.Tree
{
    public class TreeNode
    {
        public int Val;
        public TreeNode Left;
        public TreeNode Right;
    }
}
