﻿using System.Collections.Generic;

namespace BasicAlgorithmsCs.Tree
{
    public class TreeComparer
    {
        public static bool HasSameLeaves(TreeNode a, TreeNode b)
        {
            var aLeaves = DepthFirstSearch(a).GetEnumerator();
            var bLeaves = DepthFirstSearch(b).GetEnumerator();

            for (;;)
            {
                if (aLeaves.Current != bLeaves.Current) return false;

                var hasNext = aLeaves.MoveNext();
                if (hasNext != bLeaves.MoveNext()) return false;

                if (!hasNext) return true;
            }
        }

        private static IEnumerable<int> DepthFirstSearch(TreeNode node)
        {
            if (node.Left == null)
            {
                if (node.Right == null)
                {
                    yield return node.Val;
                }
            }
            else
            {
                foreach (var leafVal in DepthFirstSearch(node.Left))
                {
                    yield return leafVal;
                }
            }

            if (node.Right != null)
            {
                foreach (var leafVal in DepthFirstSearch(node.Right))
                {
                    yield return leafVal;
                }
            }
        }
    }
}
